# gitlab-backup-cookbook

LVM+Rsync based backups for GitLab servers.

Looking for the gitlab-ebs-snapshot script? See
[doc/gitlab-ebs-snapshot.md](doc/gitlab-ebs-snapshot.md).

# Previous backup system (Rsync + LVM):

## Architecture

The production GitLab server is storing all its files on a single, LVM-backed
filesystem (mounted at `/var/opt/gitlab`). Using cron, we create a daily LVM
snapshot of the filesystem, which we mount read-only on the production server
at `/mnt/gitlab_backup`. Each day the previous snapshot gets unmounted and
destroyed.

The read-only snapshot is served up from the production server using the Rsync
daemon. Now the backup server can use Rsync to pull in the files as they exist
in the snapshot. After that it is up to the backup server how to keep history.
In our case, we use a FreeBSD server with a large ZFS pool. We keep historic
backups using ZFS snapshots.

To restore a backup, we can mount any of our ZFS snapshots on the FreeBSD
server, and use Rsync/SSH to push the files from the backup server to the (new)
production server.

## Restore instructions

See [RESTORE.md](files/default/RESTORE.md) or `/opt/gitlab-backup/RESTORE.md`
on the backup server.

## Check backup sizes on the FreeBSD server

Production backups are stored in the ZFS filesystem
`gitlab_backup/gitlab_data`. To list the name, space used and creation time of
the last 10 backups you can run the following command.

```
[jacobvosmaer@stallman ~]$ zfs list -t all -r -o name,used,creation gitlab_backup/gitlab_data | tail
gitlab_backup/gitlab_data@1405440006     4.74G  Tue Jul 15 21:55 2014
gitlab_backup/gitlab_data@1405468809     4.38G  Wed Jul 16  5:26 2014
gitlab_backup/gitlab_data@1405497606     4.67G  Wed Jul 16 14:18 2014
gitlab_backup/gitlab_data@1405526406     4.69G  Wed Jul 16 21:51 2014
gitlab_backup/gitlab_data@1405555209     4.54G  Thu Jul 17  5:38 2014
gitlab_backup/gitlab_data@1405584006     4.76G  Thu Jul 17 14:07 2014
gitlab_backup/gitlab_data@1405612806     6.13G  Thu Jul 17 21:55 2014
gitlab_backup/gitlab_data@1405641609     4.65G  Fri Jul 18  5:38 2014
gitlab_backup/gitlab_data@1405958402     4.85G  Mon Jul 21 22:43 2014
gitlab_backup/gitlab_data@1405987210     57.2M  Tue Jul 22  5:49 2014
```

Note that in the example above we had an issue where our backup server was
offline for four days, hence the missing backups between July 18 and July 21.

At the moment (July 2014), a typical backup is just under 5 GB. The
`@1405987210` numbers are the times the production snapshots were made (modulo
a few seconds). The times on the right are the times the backup was transferred
to the FreeNAS server. That means that we see here that the last production
snapshot was made around 22 jul 2014 00:00:10 UTC, and the corresponding
FreeNAS snapshot was made 5:49 hours later.

If you run this command in between backups, the 'USED' attribute for the last
snapshot will be 0 because there is no difference between the current contents
of `gitlab_backup/gitlab_data` and the last snapshot. If you run the command
while a backup is in progress, the 'USED' size of the last snapshot indicates
how much new data has been written in the filesystem so far.

## gitlab-rotate-backup installation

```
# one-time manual step
sudo touch /var/opt/gitlab/backup.txt
```

The cron job rotates the backup snapshot every 8 hours. If the
`/var/opt/gitlab/backup.txt` file does not exist, the cron job unmounts
`/mnt/gitlab_backup` and does nothing else.

## gitlab-pull-backup installation

The gitlab-pull-backup script is supposed to run on a FreeNAS server.

```
sudo mkdir -p /mnt/gitlab_backup/bin /mnt/gitlab_backup/etc
```

Copy the script to `/mnt/gitlab_backup/bin/gitlab-pull-backup` and make the
file executable.

Create the config file `/mnt/gitlab_backup/etc/gitlab-pull-backup.conf` based
on
[gitlab-pull-backup.conf.example](files/default/gitlab-pull-backup.conf.example).
Make sure to fill in the snitch URL.

Configure a cronjob in the FreeNAS web interface; see these screenshots.

The command is:

```
gitlab_pull_backup_config=/mnt/gitlab_backup/etc/gitlab-pull-backup.conf /mnt/gitlab_backup/bin/gitlab-pull-backup
```

![first half](doc/freenas-cron-1.png)
![second half](doc/freenas-cron-2.png)

Note that we run the backups at 0:15, 8:15 and 16:15, while the snapshots are
created at 0:00, 8:00 and 16:00.

## Firewall settings

The FreeNAS server pulls the backups from the production servers using the
rsync protocol. For this to work, the firewall on the production servers must
allow connections on port 873.

Example Chef firewall configuration:

```json
  "default_attributes": {
    "firewall": {
      "rules": [
        "backup client rsync access": {
          "port": "873",
          "source": "192.168.0.104"
        }
      ]
    }
  }
```
