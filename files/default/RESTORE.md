# Backup restore procedure

## 1. Mount a backup snapshot

List the existing snapshots with the following command:

```
# on the backup server
zfs list -t snapshot -o name,creation,used -r gitlab_backup/gitlab_data
```

Tail of example output:

```
gitlab_backup/gitlab_data@1418256002-1418374434  Fri Dec 12  8:53 2014  11.0G
gitlab_backup/gitlab_data@1418428804-1418516429  Sun Dec 14  0:20 2014  7.91G
gitlab_backup/gitlab_data@1418428804-1418605793  Mon Dec 15  1:09 2014  7.91G
gitlab_backup/gitlab_data@1418688003-1418803400  Wed Dec 17  8:03 2014  8.28G
```

We see the 'dataset' names (volume@snapshot), followed by the local snapshot
creation time and the space used by the snapshot.

In the snapshot name there are two Unix timestamps. The first is the creation
time of the origin snapshot, and the second is the creation time of the local
snapshot. Above we see that due to a glitch, we have two local snapshots whose
origin snapshot was made at 1418428804.

Suppose we want to mount the last snapshot; we would then run:

```
# on the backup server
sudo zfs clone -o readonly=on gitlab_backup/gitlab_data@1418688003-1418803400\
 gitlab_backup/restore_data
```

We are creating a ZFS clone of the snapshot, meaning that we make it visible as
a volume of its own, named 'gitlab_backup/restore_data' (note there is no
leading '/' in the volume name!). For good measure we mount the volume
read-only.

At this point you can check out the contents of the snapshot; they are mounted
at `/gitlab_backup/restore_data`. If it is not the snapshot you want, unmount
it with:

```
# on the backup server
sudo zfs destroy gitlab_backup/restore_data
```

## 2. Push data to the restore target

First, we create an SSH key pair for our admin user on the backup server.

```
# on the backup server
#
# Use a strong password!
ssh-keygen
```

Now we want to use the new key pair to gain sudo-privileged SSH acccess to the
restore target. Because the target is managed by Chef, we cannot put the the
public key in `authorized_keys`; use `authorized_keys2` instead.

**Remember to remove the authorized_keys2 file after the backup was transfered
to the restore target!**

From the backup server, test if you have SSH access with sudo to the restore
target.

```
# on the backup server
#
# This assumes that your current user on the backup server, e.g.
# 'jacobvosmaer', also exists and has sudo rights on the target server. Because
# of Chef that should be the case
ssh target.example.com sudo ls /
```

On the target, make sure that `/var/opt/gitlab` exists.

```
# on the restore target
sudo mkdir -p /var/opt/gitlab
```

Now you can start pushing the backup data to the restore target. Make sure you
use a tmux/screen session on the backup server to launch the rsync command; it
will take a long time (days).

```
# on the backup server
#
# Use --rsync-path to launch the remote rsync process as root.
# Use -e to use your current user's SSH identity in spite of the leading sudo.
# Exclude the redis directory because we don't want to replay any jobs that
# were in the Sidekiq queue at the time of the backup.
sudo rsync \
  --rsync-path='/usr/bin/sudo /usr/bin/rsync' \
  -e "ssh -l $(whoami) -i $(echo ~)/.ssh/id_rsa" \
  --exclude=redis/ \
  -av --delete \
  /gitlab_backup/restore_data/ target.example.com:/var/opt/gitlab/
```

On the restore target, install the omnibus package that was in use at the time
of the backup. (The chef-repo commit log should tell you what the package was.)

You will have to use the same `gitlab.rb` as on the production servers on the
target, or at least the same uid/gid settings for the users created by
omnibus-gitlab, because the numerical uids/gids get preserved by rsync.


## 3. Clean up

Unmount the ZFS snapshot on the backup server.

```
# on the backup server
sudo zfs destroy gitlab_backup/restore_data
```

Remove the `authorized_keys2` file from the restore target.

```
# on the restore target
rm ~/.ssh/authorized_keys2
```
