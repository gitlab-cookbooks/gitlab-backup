argparse==1.2.1
awscli==1.7.5
bcdoc==0.12.2
botocore==0.86.0
colorama==0.2.5
docutils==0.11
jmespath==0.6.1
ply==3.4
pyasn1==0.1.7
python-dateutil==2.1
requests==1.2.0
rsa==3.1.2
six==1.3.0
wsgiref==0.1.2
