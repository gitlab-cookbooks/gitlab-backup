#!/usr/bin/env ruby
require 'optparse'
require 'yaml'
require 'azure'

def main(options)
  # Configure these properties
  config = YAML::load(File.open('/etc/gitlab-azure-ruby-scripts.yml'))
  Azure.management_certificate = config['management_certificate']
  Azure.subscription_id = config['subscription_id']

  # Create a virtual machine service object
  vm_management = Azure.vm_management

  # Get cloud_service_name
  cloud_service_name = ''

  vm_management.list_virtual_machines.each do | vm |
    if vm.vm_name.include?(options[:dest_node])
      cloud_service_name = vm.cloud_service_name
    end
  end

  unless cloud_service_name
    puts "Destination node not found in Azure!"
    exit
  end

  # Get snapshot ids and copy blobs
  vhd_id = 0
  text = File.open("/var/lib/gitlab-azure-snapshots/#{options[:src_node]}-#{options[:epoch]}.txt").read
  text.each_line do | line |
    vhd_id += 1
    media_link, snapshot_id = line.split(' ')

    storage_account_name = media_link.split('https://').last.split('.').first
    Azure.storage_account_name = storage_account_name
    Azure.storage_access_key = config['storage_account'][storage_account_name]
    blobs = Azure.blobs

    vhd = media_link.split('/').last

    # Copy snapshot to new disk
    copy_id, copy_status = blobs.copy_blob('vhds', "#{options[:dest_node]}-#{vhd_id}.vhd", 'vhds', vhd, {:source_snapshot => snapshot_id})
    puts copy_id, copy_status

    # Attach new disk to destination node
    vm_management.add_data_disk(options[:dest_node], cloud_service_name,
      {:source_media_link => "https://#{storage_account_name}.blob.core.windows.net/vhds/#{options[:dest_node]}-#{idx}.vhd"})
  end
end


options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: #$0 --epoch TIMESTAMP --source NODE_NAME  NODE_NAME"

  opts.on("--epoch TIMESTAMP", "Timestamp to use for restoring a snapshot") do |epoch|
    options[:epoch] = epoch
  end

  opts.on("--source NODE_NAME", "The node name which to restore") do |src_node|
    options[:src_node] = src_node.gsub('.', '-')
  end

end.parse!

options[:dest_node] = ARGV.first.gsub('.', '-')

raise "Missing NODE_NAME" unless options[:dest_node]

main(options)
