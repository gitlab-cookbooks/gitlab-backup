require 'yaml'

module Gitlab
  class Config
    LOCATION = 'East US 2'

    # Azure API credential configuration
    def self.azure
      YAML.load(File.read(azure_config_file))
    end

    # GitLab Snapshot configuration
    def self.snapshots
      YAML.load(File.read(gitlab_config_file))
    end

    private

    def self.debug?
      ENV['DEBUG']
    end

    def self.config_file(filename)
      if debug?
        File.expand_path("./#{filename}", __dir__)
      else
        "/etc/#{filename}"
      end
    end

    def self.azure_config_file
      config_file('gitlab-azure-ruby-scripts.yml')
    end

    def self.gitlab_config_file
      config_file('gitlab-azure-snapshots.yml')
    end
  end
end
