require 'azure_mgmt_compute'
require 'azure_mgmt_resources'

require_relative 'config'

module Gitlab
  class AzureClient
    def self.compute_client
      client(Azure::ARM::Compute::ComputeManagementClient)
    end

    def self.resource_client
      client(Azure::ARM::Resources::ResourceManagementClient)
    end

    private

    def self.client(provider_class)
      azure_config = Gitlab::Config.azure

      token_provider = MsRestAzure::ApplicationTokenProvider.new(
        azure_config['tenant_id'],
        azure_config['client_id'],
        azure_config['secret']
      )
      credentials = MsRest::TokenCredentials.new(token_provider)

      provider_class.new(credentials).tap do |client|
        client.subscription_id = azure_config['subscription_id']
      end
    end
  end
end
