data_bag_name = node['gitlab-backup']['pull_backup']['data_bag']
data_bag_item = node.chef_environment
environment_secrets = Hash.new
if data_bag_name && search(data_bag_name, "id:#{data_bag_item}").any?
  environment_secrets = Chef::EncryptedDataBagItem.load(data_bag_name, data_bag_item).to_hash
end

execute "pkg install -y rsync" do
  not_if "pkg info rsync"
end

script_path = '/opt/gitlab-backup/bin/gitlab-pull-backup'

directory File.dirname(script_path) do
  recursive true
end

cookbook_file 'gitlab-pull-backup' do
  path script_path
  mode 0755
end

pull_backup_conf = Chef::Mixin::DeepMerge.deep_merge(environment_secrets['gitlab-backup']['pull_backup'], node['gitlab-backup']['pull_backup'].to_hash)
template '/etc/gitlab-pull-backup.conf' do
  variables pull_backup_conf
  mode 0600
end

cron 'pull_backup' do
  weekday node['gitlab-backup']['pull_backup']['cron_weekday']
  hour node['gitlab-backup']['pull_backup']['cron_hour']
  minute node['gitlab-backup']['pull_backup']['cron_minute']
  path node['gitlab-backup']['pull_backup']['cron_path']
  command script_path
end
