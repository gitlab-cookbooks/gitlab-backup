script_path = '/opt/gitlab-backup/bin/gitlab-ebs-snapshot'

cron 'gitlab-ebs-snapshot' do
  weekday node['gitlab-backup']['ebs-snapshot']['cron_weekday']
  hour node['gitlab-backup']['ebs-snapshot']['cron_hour']
  minute node['gitlab-backup']['ebs-snapshot']['cron_minute']
  command script_path + ' >/var/log/gitlab-ebs-snapshot.log'
end
