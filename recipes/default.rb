#
# Cookbook Name:: gitlab-backup
# Recipe:: default
# License:: MIT
#
# Copyright (C) 2016 GitLab Inc.
#

include_recipe 'gitlab-backup::rsync_server'
include_recipe 'gitlab-backup::rotate_backup'
