log_output     = '/var/log/gitlab-azure-snapshots.log'
create_script  = '/opt/gitlab-backup/bin/gitlab-azure-snapshots'
cleanup_script = '/opt/gitlab-backup/bin/gitlab-azure-snapshots-cleanup'

cron 'gitlab-azure-snapshots' do
  weekday node['gitlab-backup']['azure-snapshots']['cron_weekday']
  hour    node['gitlab-backup']['azure-snapshots']['cron_hour']
  minute  node['gitlab-backup']['azure-snapshots']['cron_minute']
  command "#{create_script} >> #{log_output}"
end

cron 'gitlab-azure-snapshots-cleanup' do
  weekday node['gitlab-backup']['azure-snapshots-cleanup']['cron_weekday']
  hour    node['gitlab-backup']['azure-snapshots-cleanup']['cron_hour']
  minute  node['gitlab-backup']['azure-snapshots-cleanup']['cron_minute']
  command "#{cleanup_script} >> #{log_output}"
end

nodes = search(:node, 'roles:azure-snapshot').map(&:name).compact
template '/etc/gitlab-azure-snapshots.yml' do
  mode '0600'
  variables nodes: nodes
end

template '/etc/logrotate.d/gitlab-azure-snapshots' do
  mode '0644'
  source 'gitlab-azure-snapshots-logrotate.erb'
  variables log: log_output
end
