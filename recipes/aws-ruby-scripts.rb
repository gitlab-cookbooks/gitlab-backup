include_recipe 'gitlab-vault'
aws_ruby_scripts_conf = GitLab::Vault.get(node, 'gitlab-backup', 'aws-ruby-scripts')

case node['platform_family']
when 'debian'
  # Let's install an OS Ruby package instead of having compilers for Ruby on
  # all our AWS boxes.
  package 'ruby1.9.1'
end

gem_bin = '/usr/bin/gem'
bundle_bin = '/usr/local/bin/bundle'
bundler_version = '1.9.9'

execute "#{gem_bin} install bundler --version #{bundler_version}" do
  not_if "#{bundle_bin} --version | grep ' #{bundler_version}$'"
end

# Sync files/default/aws-ruby-scripts into /opt/gitlab-backup/aws-ruby-scripts
scripts_dir = '/opt/gitlab-backup/aws-ruby-scripts'
remote_directory scripts_dir do
  recursive true
  notifies :run, 'execute[bundle install for aws-ruby-scripts]'
end

execute 'bundle install for aws-ruby-scripts' do
  command "#{bundle_bin} install --path .bundle"
  cwd scripts_dir
  action :nothing
end

script_bin_dir = '/opt/gitlab-backup/bin'
directory script_bin_dir do
  recursive true
end

node['gitlab-backup']['aws-ruby-scripts']['scripts'].each do |script|
  template File.join(script_bin_dir, script) do
    mode '0755'
    source 'aws-ruby-wrapper.erb'
    variables(
      bundle_bin: bundle_bin,
      script: script,
      scripts_dir: scripts_dir
    )
  end
end

# Render /etc/gitlab-aws-ruby-scripts.conf, using attributes and secrets
template '/etc/gitlab-aws-ruby-scripts.conf' do
  mode '0600'
  variables aws_ruby_scripts_conf.to_hash
end
