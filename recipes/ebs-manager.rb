script_path = '/opt/gitlab-backup/bin/gitlab-ebs-manager'

cron 'gitlab-ebs-manager' do
  weekday node['gitlab-backup']['ebs-manager']['cron_weekday']
  hour node['gitlab-backup']['ebs-manager']['cron_hour']
  minute node['gitlab-backup']['ebs-manager']['cron_minute']
  command script_path
end
