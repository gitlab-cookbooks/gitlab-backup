include_recipe 'rsync::server'

rsync_serve 'gitlab-backup' do
  path             node['gitlab-backup']['snapshot_mountpoint']
  comment          'GitLab backup snapshot'
  read_only        true
  use_chroot       true
  uid              'root'
  hosts_allow      node['gitlab-backup']['backup_client_ip']
  hosts_deny       '0.0.0.0/0' # Deny all hosts
end
