include_recipe 'gitlab-vault'
azure_ruby_scripts_yml = GitLab::Vault.get(node, 'gitlab-backup', 'azure-ruby-scripts')

package 'git' # Needed for everything

# Packages for compiling Ruby
%w{
g++ gcc make libc6-dev libreadline6-dev zlib1g-dev
libssl-dev libyaml-dev libsqlite3-dev sqlite3 autoconf libgdbm-dev
libncurses5-dev automake libtool bison pkg-config libffi-dev nodejs
}.each do |pkg|
  package pkg
end

ruby_build_install_dir = '/usr/local/ruby-build'

git ruby_build_install_dir do
  repository 'https://github.com/sstephenson/ruby-build.git'
  notifies :run, 'execute[install ruby-build]', :immediately
end

execute 'install ruby-build' do
  command './install.sh'
  cwd ruby_build_install_dir
  env 'PREFIX' => '/usr/local'
  creates '/usr/local/bin/ruby-build'
end

ruby_version = node['gitlab-backup']['ruby_version']
execute "/usr/local/bin/ruby-build #{ruby_version} /usr/local" do
  not_if "/usr/local/bin/ruby --version | grep 'ruby #{ruby_version}'"
end

rubygems_version = node['gitlab-backup']['rubygems_version']
execute "/usr/local/bin/gem update --system #{rubygems_version} --no-ri --no-rdoc" do
  not_if "/usr/local/bin/gem --version | grep '^#{rubygems_version}$'"
end

bundler_version = node['gitlab-backup']['bundler_version']
execute "/usr/local/bin/gem install bundler --version #{bundler_version} --no-ri --no-rdoc" do
  not_if "/usr/local/bin/bundle --version | grep ' #{bundler_version}$'"
end

gem_bin = '/usr/local/bin/gem'
bundle_bin = '/usr/local/bin/bundle'

# Sync files/default/azure-ruby-scripts into /opt/gitlab-backup/azure-ruby-scripts
scripts_dir = '/opt/gitlab-backup/azure-ruby-scripts'
remote_directory scripts_dir do
  recursive true
  notifies :run, 'execute[bundle install for azure-ruby-scripts]'
end

execute 'bundle install for azure-ruby-scripts' do
  command "#{bundle_bin} install --path .bundle"
  cwd scripts_dir
  action :nothing
end

script_bin_dir = '/opt/gitlab-backup/bin'
directory script_bin_dir do
  recursive true
end

node['gitlab-backup']['azure-ruby-scripts']['scripts'].each do |script|
  template File.join(script_bin_dir, script) do
    mode '0755'
    source 'azure-ruby-wrapper.erb'
    variables(
      bundle_bin: bundle_bin,
      script: script,
      scripts_dir: scripts_dir
    )
  end
end

# Render /etc/gitlab-azure-ruby-scripts.yml, using attributes and secrets
template '/etc/gitlab-azure-ruby-scripts.yml' do
  mode '0600'
  variables azure_ruby_scripts_yml.to_hash
end
