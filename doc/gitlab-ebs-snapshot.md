# gitlab-ebs-snapshot

A script to let an Amazon Web Services EC2 instance take a snapshot of the
volumes attached to itself, combined with a Chef recipe that installs the
script as a cron job.

## Supported platforms

Tested on Ubuntu 14.04.

## The script

The script in
[files/default/gitlab-ebs-snapshot](../files/default/gitlab-ebs-snapshot) is a
shell script that reads its configuration from `/etc/gitlab-ebs-snapshot.conf`.
An example/template configuration file can be found in
[templates/default/gitlab-ebs-snapshot.conf](../templates/default/gitlab-ebs-snapshot.conf.erb).

The configuration file tells `gitlab-ebs-snapshot`

- which AWS credentials to use,
- which AWS region to connect to,
- and which file system to freeze/unfreeze before and after the snapshots.

## AWS IAM permissions

In order to make snapshots, gitlab-ebs-snapshot needs a set of AWS credentials.
An imperfect (too liberal) example would be:

```
{
  "Version": "YYY",
  "Statement": [
    {
      "Action": [
        "ec2:CreateSnapshot"
      ],
      "Sid": "ZZZ",
      "Resource": [
        "*"
      ],
      "Effect": "Allow"
    },
    {
      "Action": [
        "ec2:DescribeVolumes"
      ],
      "Sid": "XXX",
      "Resource": [
        "*"
      ],
      "Effect": "Allow"
    }
  ]
}
```

## Recipe gitlab-backup::ebs-snapshot

This Chef recipe will install the AWS command line tools, set up a cron job and
manage the `/etc/gitlab-ebs-snapshot.conf` configuration file.

## Attributes

```
{
  "gitlab-backup": {
    "ebs-snapshot": {
      "chef_vault": "my-vault",
      "chef_vault_item": "my-item",
      "freeze": "/var/opt/gitlab",
      "cron_weekday": "0",
      "cron_hour": "10",
      "cron_minute": "0",
      "cron_path": "/opt/gitlab-backup/ebs-snapshot/bin:/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin",
      "aws_default_region": "us-east",
      "aws_access_key_id": "AKIAxxx",
      "aws_secret_access_key": "secret"
    }
  }
}
```

- `freeze`: set this to `null` if you do not want to use `fsfreeze` before/after taking snapshots. Default value: `/var/opt/gitlab`.


## Secrets

It is not recommended to store secrets such as AWS credentials as role
attributes on a Chef server. This recipe allows you to mix in encrypted data
from a Chef Vault into the node attributes at runtime.

Create a vault time with the attributes you want to keep secret:

```
{
  "gitlab-backup": {
    "ebs-snapshot": {
      "aws_secret_access_key": "secret"
    }
  }
}
```

And create a Chef role which tells the ebs-snapshots recipe to look in the
correct vault:

```
{
  "name": "ebs-snapshot",
  "description": "Make weekly EBS snapshots",
  "json_class": "Chef::Role",
  "default_attributes": {
    "gitlab-backup": {
      "chef_vault": "my-vault",
      "chef_vault_item": "my-item",
      "aws_default_region": "us-east",
      "aws_access_key_id": "AKIAxxx",
      "aws_secret_access_key": "provided by Chef Vault"
    }
  },
  "override_attributes": {

  },
  "chef_type": "role",
  "run_list": [
    "recipe[gitlab-backup::ebs-snapshot]"
  ],
  "env_run_lists": {

  }
}
```

Now when the recipe runs, the value `provided by Chef Vault` will be
overwritten with the actual secret.

## Restoring

There is an example restore script in this repo:
`tools/bin/attach-backup-to-instance`. It contains hard-coded values for
theo.gitlab.com and assumes there are no more than 21 drives in the backup set.

### Set up AWS access on your local machine

You will need AWS credentials so that you can do EC2 API calls from your
laptop.

You can manage credentials by clicking on your user at
https://console.aws.amazon.com/iam/home?region=eu-central-1#users .

```
(
  umask 077
  mkdir -p ~/.aws
  credentials=~/.aws/credentials
  if ! [ -f "${credentials}" ] ; then
    cat > "${credentials}" <<EOF
[default]
aws_access_key_id = foo
aws_secret_access_key = bar
EOF
  fi
)

# Now replace 'foo' and 'bar' with your actual access key ID and secret access
# key
vi ~/.aws/credentials
```

### Removing existing data EBS drives (optional)

Before restoring you need theo.gitlab.com or an instance like it, with no extra
EBS drives attached.

If you want to attach the (data) EBS drives from theo.gitlab.com run the
following commands before detaching the drives in the AWS web interface.

```
# Check if there are data drives attached at all
lsblk

# Shut down gitlab
sudo gitlab-ctl stop

# Repeat until all services are 'down'
sudo gitlab-ctl status

# Unmount the filesystem, may need some retries
sudo umount /var/opt/gitlab

# Important! Disable the LVM logical volume
sudo vgchange -an gitlab_vg
```

Now you can detach the drives in the AWS web interface.
https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Volumes:sort=desc:createTime

### Attaching a backup set to an instance

First look for a backup set 'description' in the [AWS
console](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Snapshots:sort=startTime).
Example: `gitlab-ebs-snapshot backup 2015-03-02 10:28 UTC+0000`.

Next you need to find the EC2 instance-id of the server you are attaching the
backup set to (example: `i-123abc`).
https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Instances:sort=instanceId

```
$ cd tools
$ bundle install
<snip>
$ bundle exec bin/attach-backup-to-instance  i-63e94ea2 'gitlab-ebs-snapshot backup 2015-03-02 10:28 UTC+0000'
Creating volume from snap-9c1e0c19
<snip>
Creating volume from snap-2f1bd727
Waiting for volumes to become available
Attaching vol-9560448c to i-63e94ea2
<snip>
Attaching vol-ad6044b4 to i-63e94ea2
```

When the script finishes succesfully, SSH into the instance and run:

```
# LVM support
sudo apt-get install -y lvm2
# look for gitlab_vg on the attached drives
sudo vgchange -ay gitlab_vg
# make sure the mountpoint exists
sudo mkdir -p /var/opt/gitlab
# mount the logical volume at /var/opt/gitlab
sudo mount /dev/gitlab_vg/gitlab_com /var/opt/gitlab
```

If the script finishes succesfully but some of the volumes are failing to
attach to the instance (spinners in the AWS console), force-detach the
individual volumes and re-attach them at new drive letters going down starting
from `/dev/sdz`.
